package com.talixa.fov.shared;

public class FieldOfViewConstants {

	public static final String VERSION = "1.1";
	
	public static final String TITLE_MAIN = "Field of View";	
	public static final String TITLE_ABOUT = "About " + TITLE_MAIN;
	
	public static final String MENU_FILE = "File";	
	public static final String MENU_EXIT = "Exit";
	public static final String MENU_HELP = "Help";
	public static final String MENU_ABOUT = "About";
	
	public static final String LABEL_OK = "Ok";	
	public static final String LABEL_CANCEL = "Cancel";
		
	public static final int APP_WIDTH = 530;
	public static final int APP_HEIGHT = 600;
	
	public static final int BORDER = 10;
	public static final int BORDER_SMALL = 5;
	
	public static final String ICON = "res/icon.jpg";
	public static final String ABOUT = "res/about.jpg";
}
