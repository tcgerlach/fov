package com.talixa.fov.helper;

import java.awt.Point;

public class CheckFov {
		
	public static boolean canSee(Point observer, Point object, int facing, int fov) {
		float opposite = observer.y - object.y;
		float adjacent = object.x - observer.x;		
		double angleToObject = Math.atan(opposite/adjacent);
						
		// Remember that coordinates in Java start with 0,0 in top left
		if (object.x >= observer.x && object.y >= observer.y) {
			// Quadrant 4
			angleToObject += (2*Math.PI);
		} else if (object.x  <= observer.x && object.y >= observer.y) {
			// Quadrant 3
			angleToObject += Math.PI;			
		} else if (object.x <= observer.x && object.y <= observer.y) {
			// Quadrant 2
			angleToObject += Math.PI;
		} else {
			// Quadrant 1
			// DO NOTHING
		}
		angleToObject *= (180/Math.PI);
		
		double maxObserverAngle = (facing + fov/2);
		double minObserverAngle = (facing - fov/2);
		
		return angleToObject >=  minObserverAngle && angleToObject <= maxObserverAngle;
	}
}
