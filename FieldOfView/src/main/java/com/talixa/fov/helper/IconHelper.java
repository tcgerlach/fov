package com.talixa.fov.helper;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;

import com.talixa.fov.shared.FieldOfViewConstants;

public class IconHelper {

	public static void setIcon(Window w) {
		ClassLoader cl = IconHelper.class.getClassLoader();	
		Image im = Toolkit.getDefaultToolkit().getImage(cl.getResource(FieldOfViewConstants.ICON));
		w.setIconImage(im);
	}
}
