package com.talixa.fov.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import javax.swing.JPanel;

import com.talixa.fov.helper.CheckFov;

@SuppressWarnings("serial")
public class FOVPanel extends JPanel {

	private static final double CONVERT_TO_RAD = (Math.PI/180);
	
	// height/width of panel
	private static final int WIDTH = 512;
	private static final int HEIGHT = 512;
	private static final int STATUS_HEIGHT = 20;
	
	private static final int POINT_SIZE = 1;
	private static final int DEFAULT_FOV = 25;
	private static final int MAX_FOV = 80;
	private static final int MIN_FOV = 5;
	
	// position of point
	private int x = 256;
	private int y = 256;
	private int targetX = 20;
	private int targetY = 20;	
	private int fov = DEFAULT_FOV;
	private int direction = 0;
	
	// current state of movement & direction change
	private boolean isMovingLeft = false;
	private boolean isMovingRight = false;
	private boolean isMovingUp = false;
	private boolean isMovingDown = false;
	private boolean isLookingLeft = false;
	private boolean isLookingRight = false;
	private boolean isIncreasing = false;
	private boolean isDecreasing = false;
	private boolean isTargetMovingLeft = false;
	private boolean isTargetMovingRight = false;
	private boolean isTargetMovingUp = false;
	private boolean isTargetMovingDown = false;
	
	public FOVPanel() {
		this.setSize(WIDTH,HEIGHT+STATUS_HEIGHT);
		
		// setup auto refresh timer
		Timer t = new Timer(40, new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				repaint();				
			}
		});
		t.start();
	}

	@Override
	public void paint(Graphics g) {		
		super.paint(g);
		
		g.setColor(Color.BLACK);
		
		// draw border around fov
		g.drawLine(0, 0, 0, HEIGHT);
		g.drawLine(0, HEIGHT, WIDTH, HEIGHT);
		g.drawLine(WIDTH, HEIGHT, WIDTH, 0);
		g.drawLine(0, 0, WIDTH, 0);
						
		// get new coordinates for person
		updateCoordinates();
		
		// draw 
		g.drawOval(x,y,POINT_SIZE,POINT_SIZE);
		
		// draw FOV 
		int highAngle = direction + (fov/2);	// this is the higher of the two angles measured in degrees
		if (highAngle >= 360) {
			highAngle -= 360;
		}
		Point p1 = getBorderPoint(highAngle);
		
		int lowAngle = direction - (fov/2);
		if (lowAngle < 0) {
			lowAngle += 360;
		}
		Point p2 = getBorderPoint(lowAngle);			
		
		g.drawLine(x, y, p1.x, p1.y);
		g.drawLine(x, y, p2.x, p2.y);
		
		// fill FOV area
		Polygon p = new Polygon();
		p.addPoint(x, y);			// observer
		p.addPoint(p1.x, p1.y);		// first border point
		
		// if we are looking at a corner, add that point
		if (CheckFov.canSee(new Point(x,y), new Point(0,0), direction, fov)) {
			p.addPoint(0, 0);
		} else if (CheckFov.canSee(new Point(x,y), new Point(512,0), direction, fov)) {
			p.addPoint(512, 0);
		} else if (CheckFov.canSee(new Point(x,y), new Point(0,512), direction, fov)) {
			p.addPoint(0, 512);
		} else if (CheckFov.canSee(new Point(x,y), new Point(512,512), direction, fov)) {
			p.addPoint(512, 512);
		}
		
		p.addPoint(p2.x, p2.y); 	// second border point
		
		// fill fov area
		g.setColor(Color.RED);
		g.fillPolygon(p);
		
		// clear overflow
		g.clearRect(0, 513, 513, 1024);
		g.clearRect(513,0,1024,1024);
		
		// add status info & sample target
		g.setColor(Color.BLACK);		
		g.drawOval(targetX, targetY, 5, 5);	
		
		// add text data
		boolean canSee = CheckFov.canSee(new Point(x,y), new Point(targetX,targetY), direction, fov);
		String label = "Person: " + x + "," + y  + " Target: " + targetX + "," + targetY + " looking: " + direction + "\u00B0 fov: " + fov + "\u00B0 target visible: " + canSee;
		g.drawString(label, 0, HEIGHT+STATUS_HEIGHT);
	}
	
	private Point getBorderPoint(int angle) {
		Point p = new Point(); 
		if (angle >= 0 && angle < 90) {
			// quadrant 1
			double adj = WIDTH - x;			
			double tan = Math.tan(angle * CONVERT_TO_RAD);
			double opp = tan * adj;
			p.x = WIDTH;
			p.y = y - (int)opp;		
		} else if (angle >= 90 && angle < 180) {
			// quadrant 2
			double adj = y;
			double tan = Math.tan((angle-90) * CONVERT_TO_RAD);
			double opp = tan * adj;			
			p.x = x - (int)opp;
			p.y = 0;
		} else if (angle >= 180 && angle < 270) {
			// quadrant 3\
			double adj = x;			
			double tan = Math.tan((angle-180) * CONVERT_TO_RAD);
			double opp = tan * adj;
			p.x = 0;
			p.y = y + (int)opp;
		} else if (angle >= 270 && angle < 360) {
			// quadrant 4			
			double adj = HEIGHT - y;
			double tan = Math.tan((angle-270) * CONVERT_TO_RAD);
			double opp = tan * adj;
			p.x = x + (int)opp;
			p.y = HEIGHT;																 				
		}
				
		return p;
	}
	
	public void moveLeft(boolean isMoving) {
		this.isMovingLeft = isMoving;
	}
	
	public void moveRight(boolean isMoving) {
		this.isMovingRight = isMoving;
	}
	
	public void moveUp(boolean isMoving) {
		this.isMovingUp = isMoving;
	}
	
	public void moveDown(boolean isMoving) {
		this.isMovingDown = isMoving;
	}
	
	public void moveTargetLeft(boolean isMoving) {
		this.isTargetMovingLeft = isMoving;
	}
	
	public void moveTargetRight(boolean isMoving) {
		this.isTargetMovingRight = isMoving;
	}
	
	public void moveTargetUp(boolean isMoving) {
		this.isTargetMovingUp = isMoving;
	}
	
	public void moveTargetDown(boolean isMoving) {
		this.isTargetMovingDown = isMoving;
	}
	
	public void lookLeft(boolean isLooking) {
		this.isLookingLeft = isLooking;
	}
	
	public void lookRight(boolean isLooking) {
		this.isLookingRight = isLooking;
	}
	
	public void zoomOut(boolean zoomOut) {
		this.isIncreasing = zoomOut;
	}
	
	public void zoomIn(boolean zoomIn) {
		this.isDecreasing = zoomIn;
	}
	
	private void updateCoordinates() {
		// increment vars
		if (isMovingLeft) 	{x -= 1;}
		if (isMovingRight) 	{x += 1;}
		if (isMovingDown) 	{y += 1;} 
		if (isMovingUp) 	{y -= 1;}
		
		if (isTargetMovingLeft) 	{targetX -= 1;}
		if (isTargetMovingRight) 	{targetX += 1;}
		if (isTargetMovingDown) 	{targetY += 1;} 
		if (isTargetMovingUp) 		{targetY -= 1;}
		
		if (isLookingLeft) 	{direction += 1;}
		if (isLookingRight) {direction -= 1;}
		
		if (isIncreasing) 	{++fov;}
		if (isDecreasing) 	{--fov;}
		
		// keep variables in range
		x = clamp(0,WIDTH,x);
		y = clamp(0,HEIGHT,y);
		targetX = clamp(0,WIDTH,targetX);
		targetY = clamp(0,HEIGHT,targetY);
		
		// keep direction in range 0 - 360
		if (direction >= 360) 	{direction -= 360;}
		if (direction < 0) 		{direction += 360;}				
		if (fov > MAX_FOV)	 	{fov = MAX_FOV;}
		if (fov < MIN_FOV) 		{fov = MIN_FOV;}
	}
	
	private int clamp(int min, int max, int value) {
		if (value < min) {
			return min;
		} else if (value > max) {
			return max;
		} else {
			return value;
		}
	}
}
