package com.talixa.fov;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import com.talixa.fov.frames.FrameAbout;
import com.talixa.fov.helper.IconHelper;
import com.talixa.fov.listeners.ExitActionListener;
import com.talixa.fov.shared.FieldOfViewConstants;
import com.talixa.fov.widgets.FOVPanel;

public class FieldOfView {
	
	private static JFrame frame;
	private static FOVPanel fov;
			
	private static void createAndShowGUI() {
		frame = new JFrame(FieldOfViewConstants.TITLE_MAIN);
		
		// set close functionality 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						
		// set icon
		IconHelper.setIcon(frame);					
		
		// add menus
		addMenus();		
		
		// add panel to house fov display
		fov = new FOVPanel();		
		frame.add(fov);
		
		// Listen for key commands
		frame.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent k) {
				// DO NOTHING
			}
			
			@Override
			public void keyReleased(KeyEvent k) {
				if (k.getKeyCode() == KeyEvent.VK_UP) {
					fov.moveUp(false);
				} else if (k.getKeyCode() == KeyEvent.VK_DOWN) {
					fov.moveDown(false);
				} else if (k.getKeyCode() == KeyEvent.VK_RIGHT) {
					fov.moveRight(false);
				} else if (k.getKeyCode() == KeyEvent.VK_LEFT) {
					fov.moveLeft(false);
				} else if (k.getKeyCode() == KeyEvent.VK_A) {
					fov.lookLeft(false);
				} else if (k.getKeyCode() == KeyEvent.VK_S) {
					fov.lookRight(false);
				} else if (k.getKeyCode() == KeyEvent.VK_Z) {
					fov.zoomIn(false);
				} else if (k.getKeyCode() == KeyEvent.VK_X) {
					fov.zoomOut(false);
				} else if (k.getKeyCode() == KeyEvent.VK_NUMPAD8) { 
					fov.moveTargetUp(false);
				} else if (k.getKeyCode() == KeyEvent.VK_NUMPAD4) { 
					fov.moveTargetLeft(false);
				} else if (k.getKeyCode() == KeyEvent.VK_NUMPAD2) { 
					fov.moveTargetDown(false);
				} else if (k.getKeyCode() == KeyEvent.VK_NUMPAD6) { 
					fov.moveTargetRight(false);
				}			
			}
			
			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyCode() == KeyEvent.VK_UP) {
					fov.moveUp(true);
				} else if (k.getKeyCode() == KeyEvent.VK_DOWN) {
					fov.moveDown(true);
				} else if (k.getKeyCode() == KeyEvent.VK_RIGHT) {
					fov.moveRight(true);
				} else if (k.getKeyCode() == KeyEvent.VK_LEFT) {
					fov.moveLeft(true);
				} else if (k.getKeyCode() == KeyEvent.VK_A) {
					fov.lookLeft(true);
				} else if (k.getKeyCode() == KeyEvent.VK_S) {
					fov.lookRight(true);
				} else if (k.getKeyCode() == KeyEvent.VK_Z) {
					fov.zoomIn(true);
				} else if (k.getKeyCode() == KeyEvent.VK_X) { 
					fov.zoomOut(true);
				} else if (k.getKeyCode() == KeyEvent.VK_NUMPAD8) { 
					fov.moveTargetUp(true);
				} else if (k.getKeyCode() == KeyEvent.VK_NUMPAD4) { 
					fov.moveTargetLeft(true);
				} else if (k.getKeyCode() == KeyEvent.VK_NUMPAD2) { 
					fov.moveTargetDown(true);
				} else if (k.getKeyCode() == KeyEvent.VK_NUMPAD6) { 
					fov.moveTargetRight(true);
				}			
			}
		});
					
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(FieldOfViewConstants.APP_WIDTH,FieldOfViewConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (FieldOfViewConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (FieldOfViewConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);
	}
			
	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(FieldOfViewConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);
		
		JMenuItem exitMenuItem = new JMenuItem(FieldOfViewConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		fileMenu.add(exitMenuItem);			
		
		//*******************************************************************************
		// Setup help menu
		JMenu helpMenu = new JMenu(FieldOfViewConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		JMenuItem aboutMenuItem = new JMenuItem(FieldOfViewConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);				
			}						
		});
		helpMenu.add(aboutMenuItem);

		//*******************************************************************************
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);	
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}
		
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {			
			public void run() {
				createAndShowGUI();
			}
		});
	}			
}
